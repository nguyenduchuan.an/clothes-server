'use strict';
var mongoose = require('mongoose');

var Schema = mongoose.Schema;

var ProductSchema = new Schema ({
    name:{
        type: String,
        required: true
    },
    price:{
        type: Number,
        required: true
    },
    description:{
        type: String,
        default: ""
    },
    imageUrl:{
        type: String,
        required: true
    },
    categoryId: Schema.ObjectId
});

// a setter

ProductSchema.path("name").set((inputString) => {
    return inputString[0].toUpperCase() + inputString.slice(1);
});
module.exports = mongoose.model("Product", ProductSchema);