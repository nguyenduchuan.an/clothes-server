var router = global.router;
let Product = require('../models/ProductModal');
var mongoose = require('mongoose');
const { options } = require('../app');
const { update } = require('../models/ProductModal');
let fs = require('fs');
// GET all product
router.get('/products' , function (req, res, next) {
	 // Website you wish to allow to connect
	 res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

	 // Request methods you wish to allow
	 res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
 
	 // Request headers you wish to allow
	 res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
 
	 // Set to true if you need the website to include cookies in the requests sent
	 // to the API (e.g. in case you use sessions)
	 res.setHeader('Access-Control-Allow-Credentials', true);

	// res.end("=> List product");
	Product.find({}).limit(100).sort({name: 1}).select({
		name: 1,
		price: 1,
		description: 1,
		imageUrl: 1
	}).exec((err, products) => {
		if(err){
			res.json({
				result: "false",
				data: [],
				messege: `Error is: ${err}`
			});
		}else{
			res.json({
				result: "ok",
				data: products,
				count: products.length,
				messege: "Query list of products successfully"
			});
		}
	});
});
// Insert product
router.post('/insert_new_product', function (req, res, next) {
	// Website you wish to allow to connect
	res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

	// Request methods you wish to allow
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

	// Request headers you wish to allow
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

	// Set to true if you need the website to include cookies in the requests sent
	// to the API (e.g. in case you use sessions)
	res.setHeader('Access-Control-Allow-Credentials', true);
	console.log('fsdjfhsjdh')
	const newProduct = new Product({
		name: req.body.name,
		price: req.body.price,
		description: req.body.description,
		imageUrl: req.body.imageUrl,
	});
	newProduct.save((err) => {
		if (err) {
			res.json({
				result: "failed",
				data: {},
				messege: `Error is : ${err}`
			});
		}else{
			res.json({
				result: "ok",
				data: {
					name: req.body.name,
					price: req.body.price,
					description: req.body.description,
					imageUrl: req.body.imageUrl,
					messege: "Insert new product successfully"
				}
			})
		}
	})
});

// Get product width id
router.get('/product_width_id', function(req, res, next){
	// Chuyen id ve dang objectID
	Product.findById(require('mongoose').Types.ObjectId(req.query.product_id),(err, product) => {
		if(err){
			res.json({
				result: "false",
				data: {},
				messege: `Error is: ${err}`
			});
		}else{
			res.json({
				result: "ok",
				data: product,
				messege: "Query product by ID successfully"
			})
		}
	});
});

// Get product width criteria
router.get('/product_width_criteria', function(req, res, next){
	if(!req.query.name){
		res.json({
			result: "Faild",
			data: [],
			messege: "Input parameters is wrong! 'name' must be not NULL"
		})
	}
	let criteria = {
		name: new RegExp('^' + req.query.name + '$', "i"), // "i" = case-insensitive
	}
	const limit = parseInt(req.query.limit) > 0 ? parseInt(req.query.limit) : 100;
	Product.find(criteria).limit(limit).sort({name: 1}).select({
		name: 1,
		price: 1,
		description: 1,
		imageUrl: 1
	}).exec((err, products) => {
		if(err){
			res.json({
				result: "false",
				data: [],
				messege: `Error is: ${err}`
			});
		}else{
			res.json({
				result: "ok",
				data: products,
				count: products.length,
				messege: "Query product by ID successfully"
			})
		}
	})
});

// Update product
router.put('/update_product', function(req, res, next){
	let conditions = {}; //serch record to update
	if(mongoose.Types.ObjectId.isValid(req.body.product_id) == true){
		conditions._id = mongoose.Types.ObjectId(req.body.product_id);
	}else{
		res.json({
			result: "failed",
			data: {},
			messege: "You must enner product_id to update"
		});
	}

	let newValues = {};
	// Check name
	if(req.body.name && req.body.name.length > 2){
		newValues.name = req.body.name;
	}
	if (req.body.description && req.body.description.length > 2) {
        newValues.description = req.body.description;
    }
    //Update image
    if (req.body.image_name && req.body.image_name.length > 0) {
        //Ex: http://localhost:3001/open_image?image_name=upload_e2312e497df8c230b4896fa3b43bb543.jpg
        const serverName = require("os").hostname();
        const serverPort = require("../app").settings.port;
        newValues.imageUrl = `${serverName}:${serverPort}/open_image?image_name=${req.body.image_name}`
    }
	const options = {
		new: true,
		multi: true
	}
	if(mongoose.Types.ObjectId.isValid(req.body.category_id) == true){
		newValues.categoryId = mongoose.Types.ObjectId(req.body.category_id);
	}

	Product.findOneAndUpdate(conditions, {$set: newValues}, options, (err, updatedProduct) => {
		if(err){
			res.json({
				result: "failed",
				data: {},
				messege: `Cannot update exiting Product. Error is: ${err}`
			});
		}else{
			res.json({
				result: "ok",
				data: updatedProduct,
				messege: "Update product successfully"
			})
		}
	})
});
// Upload image
router.post('/upload_images', (req, res, next) => {
	let formidable = require('formidable');
	// parse file upload
	var form = new formidable.IncomingForm();
	form.uploadDir = "./uploads";
	form.keepExtensions = true;
	form.maxFieldsSize = 10 * 1024 * 1024; //10mb
	form.multiples = true;
	form.parse(req, (err, fields, files) => {
		if(err){
			res.json({
				result: "failed",
				data: {},
				messege: `Cannot upload iamges. Error is ${err}`
			});
		}
		var arrayOfFiles = [];
		if(files[""] instanceof Array) {
            arrayOfFiles = files[""];
        } else {
            arrayOfFiles.push(files[""]);
        }
		if(arrayOfFiles.length > 0){
			var fileNames = [];
			arrayOfFiles.forEach((eachFile) => {
				// fileNames.push(eachFile.path)
				fileNames.push(eachFile.path.split('\\')[1]);
			});
			res.json({
				result: "ok",
				data: fileNames,
				numbOfImages: fileNames.length,
				messege: "Upload images successfully"
			});
		} else{
			res.json({
				result: "failed",
				data: {},
				numbOfImages: 0,
				messege: "No images to upload."
			});
		}
	})
});

router.get('/open_image', (req, res, next) => {
    let imageName = "uploads/" + req.query.image_name;
    fs.readFile(imageName, (err, imageData) => {
        if (err) {
            res.json({
                result: "failed",
                messege: `Cannot read image.Error is : ${err}`
            });
            return;
        }
        res.writeHead(200, {'Content-Type': 'image/jpeg'});
        res.end(imageData); // Send the file data to the browser.
    });
});

// Delete product
router.delete('/delete_product', (req, res, next) => {
    Product.findOneAndRemove({_id: mongoose.Types.ObjectId(req.body.product_id)}, (err) => {
		if (err) {
			res.json({
				result: "failed",
				messege: `Cannot delete Product with categoryId: ${req.body.product_id}. Error is : ${err}`
			});
			return;
		}
		res.json({
			result: "ok",
			messege: "Delete category and Product with categoryId successful"
		});
	});
});

module.exports = router;