var router = global.router;
let User = require('../models/UserModel');
var mongoose = require('mongoose');

// GET all product
router.get('/users' , function (req, res, next) {
  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

// res.end("=> List product");
User.find({}).limit(100).sort({email: 1}).select({
  email: 1,
   password: 1
 }).exec((err, users) => {
   if(err){
     res.json({
       result: "false",
       data: [],
       messege: `Error is: ${err}`
     });
   }else{
     res.json({
       result: "ok",
       data: users,
       count: users.length,
       messege: "Query list of products successfully"
     });
   }
 });
});

// Insert product
router.post('/insert_new_user', function (req, res, next) {
	// Website you wish to allow to connect
	res.setHeader('Access-Control-Allow-Origin', 'http://localhost:3000');

	// Request methods you wish to allow
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

	// Request headers you wish to allow
	res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

	// Set to true if you need the website to include cookies in the requests sent
	// to the API (e.g. in case you use sessions)
	res.setHeader('Access-Control-Allow-Credentials', true);

	const newUser = new User({
		email: req.body.email,
		password: req.body.password
	});
	newUser.save((err) => {
		if (err) {
			res.json({
				result: "failed",
				data: {},
				messege: `Error is : ${err}`
			});
		}else{
			res.json({
				result: "ok",
				data: {
					email: req.body.email,
					password: req.body.password,
					messege: "Insert new product successfully"
				}
			})
		}
	})
});

// Get product width criteria
router.get('/user_width_criteria', function(req, res, next){
	if(!req.query.email){
		res.json({
			result: "Faild",
			data: [],
			messege: "Input parameters is wrong! 'name' must be not NULL"
		})
	}
	let criteria = {
		email: new RegExp('^' + req.query.email + '$', "i"), // "i" = chính xác
		password: new RegExp('^' + req.query.password + '$', "i"), // "i" = case-insensitive
	}
	const limit = parseInt(req.query.limit) > 0 ? parseInt(req.query.limit) : 100;
	User.find(criteria).limit(limit).sort({email: 1}).select({
		email: 1,
		password: 1
	}).exec((err, users) => {
		if(err){
			res.json({
				result: "false",
				data: [],
				messege: `Error is: ${err}`
			});
		}else{
			res.json({
				result: "ok",
				data: users,
				count: users.length,
				messege: "Query product by ID successfully"
			})
		}
	})
});


module.exports = router;
