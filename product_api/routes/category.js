const { route } = require("./users");

var router = global.router;
let Category = require('../models/CategoryModel');
let Product = require('../models/ProductModal');
var mongoose = require('mongoose');

// Insert new category(only name)
router.post('/insert_new_category', function(req, res, next){
	const criteria = {
		name: new RegExp('^' + req.body.name.trim() + '$', "i"), // "i" = case-insensitive
	}
	Category.find(criteria).limit(1).exec((err, categories) => {
		if(err){
            res.json({
                result: "failed",
                data: [],
                messege: `Error is: ${err}`
            });
        }else{
            // If it exit , do not allow to insert.
            if(categories.length > 0){
                res.json({
                    result: "failed",
                    data: [],
                    messege: "Cannot insert because category name exits."
                });
            }else{
                const newCategory = new Category({
                    name: req.body.name,
                    description: req.body.description
                });
                newCategory.save((err, addedCategory) => {
                    if(err){
                        res.json({
                            result: "failed",
                            data: {},
                            messege: `Error is: ${err}`
                        });
                    }else{
                        res.json({
                            result: "ok",
                            data: addedCategory,
                            messege: "Insert new category successfully"
                        })
                    }
                })
            }
        }
	})
});

// Xóa phần theo id category 
router.delete('/delete_a_category', (req, res, next) => {
    Category.findOneAndRemove({_id: mongoose.Types.ObjectId(req.body.category_id)}, (err) => {
        if (err) {
            res.json({
                result: "failed",
                messege: `Cannot delete a category. Error is : ${err}`
            });
            return;
        }
        Product.findOneAndRemove({categoryId: mongoose.Types.ObjectId(req.body.category_id)}, (err) => {
            if (err) {
                res.json({
                    result: "failed",
                    messege: `Cannot delete Product with categoryId: ${req.body.category_id}. Error is : ${err}`
                });
                return;
            }
            res.json({
                result: "ok",
                messege: "Delete category and Product with categoryId successful"
            });
        });
    });
});

module.exports = router